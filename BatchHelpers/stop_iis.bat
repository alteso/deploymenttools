@ECHO OFF

SET IISMachine=%1%

sc.exe \\%IISMachine% stop w3svc

IF %ERRORLEVEL%==0 GOTO successfull

IF %ERRORLEVEL%==1062 GOTO already_stopped

ECHO ERRORLEVEL=%ERRORLEVEL%
EXIT /B %ERRORLEVEL%

:already_stopped
	EXIT /B 0

:successfull
	ECHO IIS was stopped succesfully