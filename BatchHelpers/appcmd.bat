SET targetMachineIp=%1
SET userName=%2
SET password=%3
SET cmdName=%4
SET poolName=%5

ECHO psexec.exe -i -s \\%targetMachineIp% -u %userName% -p %password% C:\Windows\system32\inetsrv\appcmd.exe %cmdName% apppool %poolName%
psexec.exe -i -s \\%targetMachineIp% -u %userName% -p %password% C:\Windows\system32\inetsrv\appcmd.exe %cmdName% apppool %poolName%